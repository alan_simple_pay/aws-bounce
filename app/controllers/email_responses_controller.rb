
require 'json'
require 'aws-sdk'
require 'openssl'
require 'httparty'
class EmailResponsesController < ApplicationController

#  skip_authorization_check                                #THREW ERROR
  skip_before_action :verify_authenticity_token

  before_action :log_incoming_message

  def bounce
    # commented out below so actually add to the DB of email_responses
#    return render json: {} #unless aws_message.authentic?
    if type != 'Bounce'
      Rails.logger.info "Not a bounce - exiting"
      return render json: {}
    end

    bounce = message['bounce']
    bouncerecps = bounce['bouncedRecipients']
    bouncerecps.each do |recp|
      email = recp['emailAddress']
      extra_info  = "status: #{recp['status']}, action: #{recp['action']}, diagnosticCode: #{recp['diagnosticCode']}"
      Rails.logger.info "Creating a bounce record for #{email}"

      EmailResponse.create ({ email: email, response_type: 'bounce', extra_info: extra_info})
    end

    render json: {}
  end

  def index
    bounce =
      %q{
        {
          "Type" : "Notification",
          "MessageId" : "2d222228-90e1-5ba1-9784-9c60222222e5",
          "TopicArn" : "arn:aws:sns:eu-west-1:863122222282:yourapp_bounces",
          "Message" : "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceSubType\":\"General\",\"bounceType\":\"Permanent\",\"bouncedRecipients\":[{\"status\":\"5.1.1\",\"action\":\"failed\",\"diagnosticCode\":\"smtp; 550 5.1.1 user unknown\",\"emailAddress\":\"bounce@simulator.amazonses.com\"}],\"reportingMTA\":\"dsn; a6-26.smtp-out.eu-west-1.amazonses.com\",\"timestamp\":\"2015-01-21T00:37:34.429Z\",\"feedbackId\":\"00000122222225fb-9a217d45-8eed-4403-b7d9-172222222be7-000000\"},\"mail\":{\"timestamp\":\"2015-01-21T00:37:33.000Z\",\"source\":\"no-reply@yourdomain.com\",\"destination\":[\"bounce@simulator.amazonses.com\"],\"messageId\":\"000222222222429c-49e59593-7f16-44e3-b5a2-1a222222222c6-000000\"}}",
          "Timestamp" : "2015-01-21T00:37:34.452Z",
          "SignatureVersion" : "1",
          "Signature" : "ZmtUp+wmfSNUW0dGkzVN9A9Q7R88KwfU32zXTHn43pppZAd6pJlwKRdH/B9Ui4M1Sd1gC1Zi2WgLtC2Xi7kf60bdi66a222222222222QKeRy1GbqWfT9kcOvm4aAFlRy0FoDu2FD9iyGPu62RsABlNzLYGNI1YnmIKwHSXXy/St4MMwwGjGLprZsRSSimM/B9VJr5WCzwMmKYr/kWGVr3pN8B5WMRuFw0pYBIemJoZugkHElEypz8KNVRxSwRN3Iz7me38mMDPCs1xiv/6IyNQXu1pdtEWkAPb26feK3SuBpPNpTQtbnaZvt7wNSw==",
          "SigningCertURL" : "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-d6d622222222222222221f4f9e198.pem",
          "UnsubscribeURL" : "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:82222222222282:yourapp_bounces:222222224f-7a0a-4e07-b34d-4a662222222226"
        }

      }
      @result = HTTParty.post("https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-d6d622222222222222221f4f9e198.pem",
    :body => bounce,
    :headers => { 'Content-Type' => 'application/json' } )
      puts @result.body.to_s
      @email_responses = EmailResponse.all
  end

  def complaint
    # commented out below so actually add to the DB of email_responses
#  return render json: {} #unless aws_message.authentic?

    if type != 'Complaint'
      Rails.logger.info "Not a complaint - exiting"
      return render json: {}
    end

    complaint = message['complaint']
    recipients = complaint['complainedRecipients']
    recipients.each do |recp|
      email = recp['emailAddress']
      extra_info = "complaintFeedbackType: #{complaint['complaintFeedbackType']}"
      EmailResponse.create ( { email: email, response_type: 'complaint', extra_info: extra_info } )
    end

    render json: {}
  end

  protected

  def aws_message
    @aws_message ||= AWS::SNS::Message.new request.raw_post
  end

  def log_incoming_message
    Rails.logger.info request.raw_post
  end

  # Weirdly, AWS double encodes the JSON.
  def message
    @message ||= JSON.parse JSON.parse(request.raw_post)['Message']
  end

  def type
    message['notificationType']
  end
end
